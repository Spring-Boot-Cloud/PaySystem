package com.so206.mapper;

import com.so206.po.SystemPayConfig;
import com.so206.po.SystemPayConfigExample;
import com.so206.po.SystemPayConfigWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SystemPayConfigMapper {
    long countByExample(SystemPayConfigExample example);

    int deleteByExample(SystemPayConfigExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SystemPayConfigWithBLOBs record);

    int insertSelective(SystemPayConfigWithBLOBs record);

    List<SystemPayConfigWithBLOBs> selectByExampleWithBLOBs(SystemPayConfigExample example);

    List<SystemPayConfig> selectByExample(SystemPayConfigExample example);

    SystemPayConfigWithBLOBs selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SystemPayConfigWithBLOBs record, @Param("example") SystemPayConfigExample example);

    int updateByExampleWithBLOBs(@Param("record") SystemPayConfigWithBLOBs record, @Param("example") SystemPayConfigExample example);

    int updateByExample(@Param("record") SystemPayConfig record, @Param("example") SystemPayConfigExample example);

    int updateByPrimaryKeySelective(SystemPayConfigWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(SystemPayConfigWithBLOBs record);

    int updateByPrimaryKey(SystemPayConfig record);
}