package com.so206.po.ext;

import com.so206.po.SystemUser;

public class SysUserExt extends SystemUser {

    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }


}
